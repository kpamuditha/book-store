<?php

namespace App\Service;

use App\Entity\Customer;
use App\Entity\Order;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;

/**
 * CheckoutService
 * Handle checkout
 */
class CheckoutService
{
    private $customer = null;
    private $cart = null;
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }
        
    /**
     * Set customer of the checkout.
     *
     * @param  mixed $cutomer
     * @return array
     */
    public function setCustomer(array $cutomer): void
    {
        $this->customer = $cutomer;
    }        
        
    /**
     * getCustomer
     *
     * @return array
     */
    public function getCustomer(): array
    {
        return $this->customer;
    }
    
    /**
     * Set cart object to the checkout process.
     *
     * @param  mixed $cart
     * @return void
     */
    public function setCart($cart): void
    {
        $this->cart = $cart;
    }

        
    /**
     * Get the cart object.
     */
    public function getCart()
    {
        return $this->cart;
    }
    
    /**
     * Persist the checkout data in to DB.
     *
     * @return Order
     */
    public function persist(): Order
    {
        $customer = new Customer();
        $customer->setFirstName($this->customer["first_name"]);
        $customer->setLastName($this->customer["last_name"]);
        $customer->setEmail($this->customer["email"]);
        $customer->setAddress($this->customer["address"]);

        $this->em->persist($customer);
        $this->em->flush();

        $order = new Order();
        $order->setCustomer($customer);
        $order->setCart($this->cart);
        $order->setCreatedAt(new DateTime("now"));
        $this->em->persist($order);
        $this->em->flush();

        return $order;
    }
}
