<?php

namespace App\Service;

use Symfony\Component\HttpFoundation\Session\SessionInterface;

/**
 * CartUtils
 * Maintain the cart status in the session.
 */
class CartUtils
{
    public const CART_SESSION_KEY = "cart";
    public const CART_COUPON_SESSION_KEY = "coupon";

    private $session;
    private $items = [];
    private $coupon = null;

    public function __construct(SessionInterface $session)
    {
        $this->session = $session;
        $this->items = $this->session->get(self::CART_SESSION_KEY, []);
        $this->coupon = $this->session->get(self::CART_COUPON_SESSION_KEY, null);
    }
        
    /**
     * Add an item to the cart.
     *
     * @param  mixed $itemId
     * @return void
     */
    public function addItem(int $itemId): void
    {
        $key = array_search($itemId, array_column($this->items, 'itemId'));
        if ($key !== false) {
            $this->items[$key]["count"] += 1;
        } else {
            $this->items[] = [
                "itemId" => $itemId,
                "count" => 1
            ];
        }

        $this->persist();
    }
        
    /**
     * Remove an item from cart.
     *
     * @param  mixed $itemId
     * @return void
     */
    public function removeItem(int $itemId): void
    {
        $key = array_search($itemId, array_column($this->items, 'itemId'));

        if ($key >= 0) {
            unset($this->items[$key]);
        }

        $this->items = array_values($this->items);
        $this->persist();
    }
    
    /**
     * Get items of cart
     *
     * @return array
     */
    public function getItems(): array
    {
        return $this->items;
    }
    
    /**
     * Persist the cart items in the session.
     *
     * @return void
     */
    private function persist(): void
    {
        $this->session->set(self::CART_SESSION_KEY, $this->items);
    }
    
    /**
     * Set a coupon to the cart.
     *
     * @param  mixed $coupon
     * @return void
     */
    public function setCoupon(string $coupon): void
    {
        $this->coupon = $coupon;
        $this->persistCoupon();
    }
    
    /**
     * Get the coupon.
     *
     * @return string
     */
    public function getCoupon(): ?string
    {
        return $this->coupon;
    }
    
    /**
     * Remove the coupon.
     *
     * @return void
     */
    public function removeCoupon(): void
    {
        $this->coupon = null;
        $this->session->remove(self::CART_COUPON_SESSION_KEY);
    }
    
    /**
     * Persist the coupon in the session.
     *
     * @return void
     */
    private function persistCoupon(): void
    {
        $this->session->set(self::CART_COUPON_SESSION_KEY, $this->coupon);
    }
    
    /**
     * Clear the cart state from session.
     *
     * @return void
     */
    public function clear()
    {
        $this->session->remove(self::CART_SESSION_KEY);
        $this->session->remove(self::CART_COUPON_SESSION_KEY);
    }
}
