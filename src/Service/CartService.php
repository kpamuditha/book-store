<?php

namespace App\Service;

use App\Entity\Book;
use App\Service\Discount\Discountable;
use Doctrine\ORM\EntityManagerInterface;

/**
 * CartService
 * Maintain the state of the cart.
 */
class CartService
{
    private $utils = null;
    private $items = [];
    private $discounts = [];
    private $em;
    public $total;
    public $discount;

    public function __construct(
        EntityManagerInterface $em
    ) {
        $this->em = $em;
    }
    
    /**
     * Initialize the cart with items and related stuffs.
     *
     * @param  mixed $cartUtils
     * @return void
     */
    public function init(CartUtils $cartUtils): void
    {
        $this->utils = $cartUtils;
        $cartItems = [];
        foreach ($cartUtils->getItems() as $item) {
            $cartItems[$item["itemId"]] = $item["count"];
        }

        $repository = $this->em->getRepository(Book::class);
        $items = $repository->findBy(["id" => array_keys($cartItems)]);
        foreach ($items as $item) {
            $item->setCount($cartItems[$item->getId()]);
            $this->addItem($item);
        }
    }
    
    /**
     * Add an item to the cart object
     *
     * @param  mixed $item
     * @return void
     */
    public function addItem(Book $item): void
    {
        $this->items[] = $item;
    }

    /**
     * Add an items of teh cart
     *
     * @param  mixed $item
     * @return void
     */
    public function getItems(): array
    {
        return $this->items;
    }
    
    /**
     * Get the price before discounts.
     *
     * @return float
     */
    public function getTotalBeforeDiscounts(): float
    {
        $total = 0;
        foreach ($this->items as $item) {
            $total += $item->getPrice() * $item->getCount();
        }

        return round($total, 2, PHP_ROUND_HALF_UP);
    }
    
    /**
     * Apply discounts to the cart.
     *
     * @param  mixed $total
     * @return float
     */
    public function applyDiscounts(float $total): float
    {
        $discount = 0;

        usort($this->discounts, function ($a, $b) {
            return $b->getPriority() - $a->getPriority();
        });

        foreach ($this->discounts as $discountable) {
            $discount += $discountable->setDiscount($this->items, $total);
            if ($discountable->terminate === 1) {
                break;
            }
        }

        return $discount;
    }
    
    /**
     * Get total of the cart (initial total - discount)
     *
     * @return float
     */
    public function getTotal(): float
    {
        $total = $this->getTotalBeforeDiscounts();
        $this->total = $total;
        $discount = $this->applyDiscounts($total);
        $this->discount = $total;
        return round($total - $discount, 2, PHP_ROUND_HALF_UP);
    }
    
    /**
     * Add a discount to the cart.
     *
     * @param  mixed $discountable
     * @return void
     */
    public function addDiscount(Discountable $discountable): void
    {
        $this->discounts[] = $discountable;
    }
    
    /**
     * Initialize the discounts for teh cart.
     *
     * @param  mixed $discounts
     * @return void
     */
    public function initDiscounts($discounts): void
    {
        foreach ($discounts as $discount) {
            $this->addDiscount($discount);
        }
    }
    
    /**
     * Return the cart in formatted way.
     *
     * @return void
     */
    public function dump(): array
    {
        $items = [];

        foreach ($this->items as $item) {
            $items[] = [
                "itemId" => $item->getId(),
                "count" => $item->getCount()
            ];
        }

        $total = $this->getTotalBeforeDiscounts();
        $discount = $this->applyDiscounts($total);

        return [
            "items" => $items,
            "total" => $total,
            "discount" => $discount
        ];
    }
}
