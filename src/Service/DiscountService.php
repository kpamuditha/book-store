<?php

namespace App\Service;

use App\Entity\Category;
use Doctrine\ORM\EntityManagerInterface;
use App\Service\Discount\CategoryDiscounts;
use App\Service\Discount\CouponDiscounts;
use App\Service\Discount\EachCategoryDiscounts;

/**
 * Handle and generate discountable objects.
 */
class DiscountService
{
    private $em;
    private $categoryDiscount;
    private $eachCategoryDiscounts;
    private $couponDiscount;

    public function __construct(
        EntityManagerInterface $em,
        CategoryDiscounts $categoryDiscount,
        EachCategoryDiscounts $eachCategoryDiscounts,
        CouponDiscounts $couponDiscount
    ) {
        $this->em = $em;
        $this->categoryDiscount = $categoryDiscount;
        $this->eachCategoryDiscounts = $eachCategoryDiscounts;
        $this->couponDiscount = $couponDiscount;
    }
    
    /**
     * Get discounts.
     *
     * @param  mixed $utils
     * @return array
     */
    public function getDiscounts(CartUtils $utils): array
    {
        $discounts = [];

        $repository = $this->em->getRepository(Category::class);
        $category = $repository->find(1);

        $this->categoryDiscount->setCategory($category);
        $this->categoryDiscount->setMinItems(5);
        $this->categoryDiscount->setPriority(1);
        $this->categoryDiscount->setDiscountPercentage(10);
        $discounts[] = $this->categoryDiscount;

        $allCategories = $repository->countAll();
        $this->eachCategoryDiscounts->setCategoryCount($allCategories);
        $this->eachCategoryDiscounts->setMinItems(5);
        $this->eachCategoryDiscounts->setPriority(2);
        $this->eachCategoryDiscounts->setDiscountPercentage(5);
        $discounts[] = $this->eachCategoryDiscounts;

        if ($utils->getCoupon()) {
            $this->couponDiscount->setDiscountPercentage(15);
            $this->couponDiscount->setDiscountPercentage(15);
            $discounts[] = $this->couponDiscount;
        }

        return $discounts;
    }
}
