<?php

namespace App\Service\Discount;

class EachCategoryDiscounts implements Discountable
{
    private $priority = 1;
    private $categoryCount = 1;
    private $minItems = 1;
    private $discountPercentage = 10;
    private $title = '';
    public $terminate = 0;

    public function setDiscountName(string $text): void
    {
        $this->title = $text;
    }

    public function setMinItems(int $minItems): void
    {
        $this->minItems = $minItems;
    }

    public function getMinItems(): int
    {
        return $this->minItems;
    }

    public function setDiscountPercentage(int $minItems): void
    {
        $this->discountPercentage = $minItems;
    }

    public function getDiscountPercentage(): int
    {
        return $this->discountPercentage;
    }

    public function setCategoryCount(int $categoryCount): void
    {
        $this->categoryCount = $categoryCount;
    }

    public function getCategoryCount(): int
    {
        return $this->categoryCount;
    }

    public function setPriority(int $priority): void
    {
        $this->priority = $priority;
    }

    public function getPriority(): int
    {
        return $this->priority;
    }

    public function setDiscount(array $items, float $total): float
    {
        $booksOfCategory = [];
        foreach ($items as $item) {
            if (array_key_exists($item->getCategory()->getId(), $booksOfCategory)) {
                $booksOfCategory[$item->getCategory()->getId()] += $item->getCount();
            } else {
                $booksOfCategory[$item->getCategory()->getId()] = $item->getCount();
            }
        }

        $eligible = count($booksOfCategory) === $this->categoryCount;

        if ($eligible) {
            foreach ($booksOfCategory as $countOfEachCategory) {
                if ($countOfEachCategory < $this->minItems) {
                    $eligible = false;
                }
            }
        }

        if ($eligible) {
            return $total * $this->discountPercentage / 100;
        }

        return 0;
    }
}
