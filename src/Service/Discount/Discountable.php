<?php

namespace App\Service\Discount;

interface Discountable
{
    public function setDiscount(array $items, float $total): float;
    public function setDiscountName(string $name): void;
    public function setPriority(int $name): void;
}
