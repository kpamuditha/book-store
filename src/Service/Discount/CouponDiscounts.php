<?php

namespace App\Service\Discount;

class CouponDiscounts implements Discountable
{
    private $priority = 999;
    private $couponCode;
    private $discountPercentage = 10;
    private $title = '';
    public $terminate = 1;

    public function setDiscountName(string $text): void
    {
        $this->title = $text;
    }

    public function setDiscountPercentage(int $minItems): void
    {
        $this->discountPercentage = $minItems;
    }

    public function getDiscountPercentage(): int
    {
        return $this->discountPercentage;
    }

    public function setCouponCode(string $couponCode): void
    {
        $this->couponCode = $couponCode;
    }

    public function getCouponCode(): string
    {
        return $this->couponCode;
    }

    public function setPriority(int $priority): void
    {
        $this->priority = $priority;
    }

    public function getPriority(): int
    {
        return $this->priority;
    }

    public function setDiscount(array $items, float $total): float
    {
        return $total * $this->discountPercentage / 100;
    }
}
