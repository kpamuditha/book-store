<?php

namespace App\Service\Discount;

use App\Entity\Category;

class CategoryDiscounts implements Discountable
{
    private $category;
    private $priority;
    private $minItems = 1;
    private $discountPercentage = 10;
    private $title = '';
    public $terminate = 0;

    public function setDiscountName(string $text): void
    {
        $this->title = $text;
    }

    public function setCategory(Category $category): void
    {
        $this->category = $category;
    }

    public function getCategory(): ?Category
    {
        return $this->category;
    }

    public function setMinItems(int $minItems): void
    {
        $this->minItems = $minItems;
    }

    public function getMinItems(): int
    {
        return $this->minItems;
    }

    public function setDiscountPercentage(int $minItems): void
    {
        $this->discountPercentage = $minItems;
    }

    public function getDiscountPercentage(): int
    {
        return $this->discountPercentage;
    }

    public function setPriority(int $priority): void
    {
        $this->priority = $priority;
    }

    public function getPriority(): int
    {
        return $this->priority;
    }

    public function setDiscount(array $items, float $_): float
    {
        $booksOfCategory = 0;
        $totalOfCategory = 0;

        foreach ($items as $item) {
            if ($item->getCategory()->getId() === $this->category->getId()) {
                $booksOfCategory += $item->getCount();
                $totalOfCategory += $item->getPrice() * $item->getCount();
            }
        }

        if ($booksOfCategory >= $this->minItems) {
            return $totalOfCategory * $this->discountPercentage / 100;
        }

        return 0;
    }
}
