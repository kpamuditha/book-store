<?php

namespace App\DataFixtures;

use App\Entity\Author;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class AuthorFixtures extends Fixture
{
    public static $fakeAuthors = [
        "William Shakespeare",
        "Agatha Christie",
        "Barbara Cartland",
        "Danielle Steel",
        "Harold Robbins",
        "Georges Simenon",
        "Roald Dahl",
        "John Grisham",
        "Beverly cleary",
        "Jroald dhal"
    ];

    public function load(ObjectManager $manager)
    {
        foreach (AuthorFixtures::$fakeAuthors as $i => $authorName) {
            $author = new Author();
            $author->setName($authorName);
            $manager->persist($author);
            $this->addReference(Author::class . '_' . $i, $author);
        }

        $manager->flush();
    }
}
