<?php

namespace App\DataFixtures;

use App\Entity\Category;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class CategoryFixtures extends Fixture
{
    public static $fakeCategories = [
        "Children",
        "Fiction"
    ];

    public function load(ObjectManager $manager)
    {
        foreach (CategoryFixtures::$fakeCategories as $i => $categoryName) {
            $category = new Category();
            $category->setName($categoryName);
            $manager->persist($category);
            $this->addReference(Category::class . '_' . $i, $category);
        }

        $manager->flush();
    }
}
