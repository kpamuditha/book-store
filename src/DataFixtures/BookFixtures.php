<?php

namespace App\DataFixtures;

use App\Entity\Author;
use App\Entity\Book;
use App\Entity\Category;
use DateTime;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Exception;

class BookFixtures extends Fixture implements DependentFixtureInterface
{
    private $fakeBooks = [[
        "King Henry VI - Arden Shakespeare",
        "The Goat, or Who Is Sylvia?: Broadway Edition",
        "Ruined",
        "The Laramie Project",
        "August : Osage County",
        "Proof : A Play"
    ], [
        "I Know Why the Caged Bird Sings",
        "The Year of Magical ThinkingOn Writing: A Memoir of the Craft",
        "The Year of Magical Thinking",
        "Angela's Ashes: A Memoir",
        "Girl, Interrupted",
        "Bossypants",
        "Just Kids",
        "Travels with Charley: In Search of America",
        "A Million Little Pieces"
    ], [
        "The Liars' Club: A Memoir",
        "Three Weeks with My Brother",
        "A Moveable Feast",
        "Scar Tissue",
        "Is Everyone Hanging Out Without Me? (And Other Concerns)",
        "Black Boy: A Record of Youth and Childhood",
        "Gift from the Sea"
    ], [
        "Naked",
        "Surprised by Joy: The Shape of My Early Life",
        "Survival in Auschwitz",
        "The Heart of a Woman",
        "Running with Scissors"
    ], [
        "Wishful Drinking",
        "Animal, Vegetable, Miracle: A Year of Food Life",
        "The Dirt: Confessions of the World's Most Notorious Rock Band",
        "Letter to My Daughter",
        "The Surgeon of Crowthorne: A Tale of Murder, Madness & the Love of Words"
    ], [
        "84, Charing Cross Road",
        "The Heroin Diaries: A Year in the Life of a Shattered Rock Star",
        "Always Running: La Vida Loca: Gang Days in L.A.",
        "Reading Lolita in Tehran: A Memoir in Books"
    ], [
        "A Heartbreaking Work of Staggering Genius",
        "Papillon",
        "Life",
        "Born Standing Up: A Comic's Life",
        "The Story of the Trapp Family Singers",
        "Journals",
        "Rain of Gold",
        "A Small Place",
        "What I Talk about When I Talk about Running: A Memoir",
        "La nuit, L'aube, Le jour",
        "The Life and Times of The Thunderbolt Kid: A Memoir",
        "Traveling Mercies: Some Thoughts on Faith",
        "Darkness Visible: A Memoir of Madness"
    ], [
        "Cash: The Autobiography of Johnny Cash",
        "The Long Hard Road Out of Hell",
        "The Complete Maus",
        "Frida: A Biography of Frida Kahlo",
        "Chronicles: Volume One",
        "Don't Let's Go to the Dogs Tonight: An African Childhood",
        "Dry: A Memoir",
        "Home: A Memoir of My Early Years",
        "The Writing Life",
        "Dolly: My Life and Other Unfinished Business",
        "Seriously... Im Kidding",
        "Tis: A Memoir"
    ], [
        "Decoded",
        "This Boy's Life: A Memoir",
        "Slash",
        "Blue Nights",
        "Lincoln"
    ]];

    private $fakeImages = [
        "https://m.media-amazon.com/images/I/51LYB1NRg5L._SL500_.jpg",
        "https://img.thriftbooks.com/api/images/i/l/1E3B2CF941452726B211CE05167005D8545CD8B7.jpg",
        "https://m.media-amazon.com/images/I/414KJDhKRML._SL500_.jpg",
        "https://img.thriftbooks.com/api/images/i/l/A595B5B064886B1321EA6414FC68925FCE55B372.jpg",
        "https://m.media-amazon.com/images/I/41BMGYE45xL._SL500_.jpg",
        "https://m.media-amazon.com/images/I/417Ao6M41bL._SL500_.jpg",
        "https://m.media-amazon.com/images/I/31Lk4u-1UQL._SL500_.jpg",
        "https://m.media-amazon.com/images/I/41vWKXnOHyL._SL500_.jpg",
        "https://m.media-amazon.com/images/I/41cZguxsjzL._SL500_.jpg",
        "https://m.media-amazon.com/images/I/41KB-4kGwzL._SL500_.jpg",
        "https://m.media-amazon.com/images/I/41qC7sN6OGL._SL500_.jpg",
        "https://m.media-amazon.com/images/I/51lVoALt-2L._SL500_.jpg",
        "https://img.thriftbooks.com/api/images/i/l/A33A6A636330BEB91D433A866F7874C8E6CD0A24.jpg",
        "https://m.media-amazon.com/images/I/51ixsUsMYiL._SL500_.jpg",
        "https://m.media-amazon.com/images/I/51s48RMlF7L._SL500_.jpg",
        "https://m.media-amazon.com/images/I/41kdPXx7diL._SL500_.jpg",
        "https://m.media-amazon.com/images/I/41NUT1+f3CL._SL500_.jpg",
        "https://m.media-amazon.com/images/I/51+ACshTCNL._SL500_.jpg",
        "https://m.media-amazon.com/images/I/41qFeL13yhL._SL500_.jpg",
        "https://m.media-amazon.com/images/I/51Y6QqomUnL._SL500_.jpg",
        "https://m.media-amazon.com/images/I/41fi014QicL._SL500_.jpg",
        "https://m.media-amazon.com/images/I/41-jTa-oEgL._SL500_.jpg",
        "https://m.media-amazon.com/images/I/61JFcSb52bL._SL500_.jpg",
        "https://m.media-amazon.com/images/I/51pkWXz1vgL._SL500_.jpg",
        "https://m.media-amazon.com/images/I/51EssumFR2L._SL500_.jpg",
        "https://m.media-amazon.com/images/I/51erLbhmoQL._SL500_.jpg",
        "https://m.media-amazon.com/images/I/51PybZSiHPL._SL500_.jpg",
        "https://m.media-amazon.com/images/I/61FKtJndIkL._SL500_.jpg",
        "https://m.media-amazon.com/images/I/511fadb9TyL._SL500_.jpg",
        "https://m.media-amazon.com/images/I/51JTiZ-6AiL._SL500_.jpg"
    ];

    private function generateRandomString($length = 10)
    {
        $characters = '01 234567 89abcdef ghijklmn opqrstu vwxyzAB CDEFGHI JKLM NOPQR STUV WXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public function load(ObjectManager $manager)
    {
        $numberOfAuthors = count(AuthorFixtures::$fakeAuthors) - 1;

        foreach ($this->fakeBooks as $authorGrouped) {
            $author = $this->getReference(Author::class . "_" . rand(1, $numberOfAuthors));

            foreach ($authorGrouped as $key => $bookTitle) {
                $book = new Book();
                $book->setAuthor($author);
                $book->setIsbn(rand(999999, 99999999));
                $book->setTitle($bookTitle);
                $book->setDescription($bookTitle . " " . $this->generateRandomString(160));
                $book->setCreatedAt(new DateTime());
                $book->setPrice(mt_rand(99, 9999) / 10);
                $book->setImage($this->fakeImages[rand(0, count($this->fakeImages) - 1)]);
                $book->setCategory($this->getReference(Category::class . "_" . rand(0, 1)));

                // $author->addBook($book);
                $manager->persist($book);
            }
        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            AuthorFixtures::class,
            CategoryFixtures::class
        ];
    }
}
