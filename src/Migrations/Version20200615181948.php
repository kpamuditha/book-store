<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200615181948 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE trn_author (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE trn_book (id INT AUTO_INCREMENT NOT NULL, author_id INT NOT NULL, category_id INT NOT NULL, isbn VARCHAR(255) NOT NULL, title VARCHAR(255) NOT NULL, description VARCHAR(255) NOT NULL, price DOUBLE PRECISION NOT NULL, created_at DATETIME NOT NULL, image VARCHAR(255) DEFAULT NULL, INDEX IDX_54D3CB1CF675F31B (author_id), INDEX IDX_54D3CB1C12469DE2 (category_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE set_catregory (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE trn_customer (id INT AUTO_INCREMENT NOT NULL, first_name VARCHAR(255) NOT NULL, email VARCHAR(255) NOT NULL, last_name VARCHAR(255) NOT NULL, address VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE trn_order (id INT AUTO_INCREMENT NOT NULL, customer_id INT NOT NULL, cart LONGTEXT NOT NULL, created_at DATETIME NOT NULL, INDEX IDX_B069F9859395C3F3 (customer_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE trn_book ADD CONSTRAINT FK_54D3CB1CF675F31B FOREIGN KEY (author_id) REFERENCES trn_author (id)');
        $this->addSql('ALTER TABLE trn_book ADD CONSTRAINT FK_54D3CB1C12469DE2 FOREIGN KEY (category_id) REFERENCES set_catregory (id)');
        $this->addSql('ALTER TABLE trn_order ADD CONSTRAINT FK_B069F9859395C3F3 FOREIGN KEY (customer_id) REFERENCES trn_customer (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE trn_book DROP FOREIGN KEY FK_54D3CB1CF675F31B');
        $this->addSql('ALTER TABLE trn_book DROP FOREIGN KEY FK_54D3CB1C12469DE2');
        $this->addSql('ALTER TABLE trn_order DROP FOREIGN KEY FK_B069F9859395C3F3');
        $this->addSql('DROP TABLE trn_author');
        $this->addSql('DROP TABLE trn_book');
        $this->addSql('DROP TABLE set_catregory');
        $this->addSql('DROP TABLE trn_customer');
        $this->addSql('DROP TABLE trn_order');
    }
}
