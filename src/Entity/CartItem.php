<?php

namespace App\Entity;

class CartItem
{
    private $count = 0;

    public function setCount(int $count): void
    {
        $this->count = $count;
    }

    public function getCount(): int
    {
        return $this->count;
    }
}
