<?php

namespace App\Controller;

use App\Service\CartService;
use App\Service\CartUtils;
use App\Service\DiscountService;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class CartController extends AbstractController
{
    private $cartUtils;
    private $cartService;
    private $discountService;

    /**
     * Constructor function
     *
     * @return void
     */
    public function __construct(
        CartUtils $cartUtils,
        CartService $cartService,
        DiscountService $discountService
    ) {
        $this->cartUtils = $cartUtils;
        $this->cartService = $cartService;
        $this->discountService = $discountService;
    }

    /**
     * Add items to the card.
     *
     * @Route("/cart/add/{id}", name="cart_add", requirements={"id"="\d+"})
     *
     * @param  mixed $id
     * @return JsonResponse
     */
    public function add(int $id): JsonResponse
    {
        try {
            $this->cartUtils->addItem($id);

            $response = new JsonResponse();
            $response->setData([
                "success" => true
            ]);

            return $response;
        } catch (Exception $ex) {
            $response = new JsonResponse();
            return $response->setData([
                "success" => false
            ]);
        }
    }
        
    /**
     * Remove item from cart.
     *
     * @Route("/cart/remove/{id}", name="cart_remove", requirements={"id"="\d+"})
     * @param  mixed $id
     * @return RedirectResponse
     */
    public function remove(int $id): RedirectResponse
    {
        try {
            $this->cartUtils->removeItem($id);

            return $this->redirectToRoute('checkout');
        } catch (Exception $ex) {
            return $this->redirectToRoute('search_books');
        }
    }
        
    /**
     * Get total of the cart.
     *
     * @Route("/cart/total", name="cart_total")
     * @return JsonResponse
     */
    public function total(): JsonResponse
    {
        try {
            $this->cartService->init($this->cartUtils);
            $discounts = $this->discountService->getDiscounts($this->cartUtils);
            $this->cartService->initDiscounts($discounts);

            $response = new JsonResponse();
            $response->setData([
                "success" => true,
                "total" => $this->cartService->getTotal()
            ]);

            return $response;
        } catch (Exception $ex) {
            $response = new JsonResponse();
            return $response->setData([
                "success" => false
            ]);
        }
    }
        
    /**
     * Add a coupon.
     *
     * @Route("/cart/coupon", name="cart_coupon")
     * @param  mixed $request
     * @return void
     */
    public function coupon(Request $request): RedirectResponse
    {
        try {
            $coupon = $request->get("coupon", null);
            if ($coupon) {
                $this->cartUtils->setCoupon($coupon);
            }
            return $this->redirectToRoute('checkout');
        } catch (Exception $ex) {
            return $this->redirectToRoute('search_books');
        }
    }

        
    /**
     * Remove the coupon.
     *
     * @Route("/cart/coupon/remove", name="cart_coupon_remove")
     * @param  mixed $request
     * @return void
     */
    public function removeCoupon(): RedirectResponse
    {
        try {
            $this->cartUtils->removeCoupon();
            return $this->redirectToRoute('checkout');
        } catch (Exception $ex) {
            return $this->redirectToRoute('search_books');
        }
    }

    /**
     * @Route("/cart", name="cart_view")
     */
    public function cart()
    {
        $this->cartService->init($this->cartUtils);
        dd($this->cartService->getItems());
    }
}
