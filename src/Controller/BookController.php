<?php

namespace App\Controller;

use App\Entity\Author;
use App\Entity\Book;
use App\Entity\Category;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class BookController extends AbstractController
{
        
    /**
     * Index page redirect to book search.
     *
     * @Route("/", name="index")
     *
     * @return void
     */
    public function index(): RedirectResponse
    {
        return $this->redirectToRoute('search_books');
    }

        
    /**
     * List the books.
     *
     * @Route("/book/list", name="search_books")
     *
     * @param  mixed $em
     * @param  mixed $request
     * @param  mixed $paginator
     * @return void
     */
    public function list(EntityManagerInterface $em, Request $request, PaginatorInterface $paginator): Response
    {
        $repository = $em->getRepository(Book::class);

        $search = [];
        if ($request->query->has("category")) {
            $search["category"] = $request->query->getInt("category");
        }

        $queryBuilder = $repository->search($search);
        
        $pagination = $paginator->paginate(
            $queryBuilder,
            $request->query->getInt("page", 1),
            $this->getParameter("app.pagination.limit")
        );

        $categories = $em->getRepository(Category::class)->findAll();

        return $this->render('search.html.twig', [
            "preFill" =>  $search,
            "pagination" => $pagination,
            "categories" => $categories
        ]);
    }
}
