<?php

namespace App\Controller;

use App\Entity\Book;
use App\Entity\Order;
use App\Form\CheckoutForm;
use App\Service\CartService;
use App\Service\CartUtils;
use App\Service\CheckoutService;
use App\Service\DiscountService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CheckoutController extends AbstractController
{
    private $cartUtils;
    private $cartService;
    private $checkoutService;
    private $discountService;

    public function __construct(
        CartUtils $cartUtils,
        CartService $cartService,
        CheckoutService $checkoutService,
        DiscountService $discountService
    ) {
        $this->cartUtils = $cartUtils;
        $this->cartService = $cartService;
        $this->checkoutService = $checkoutService;
        $this->discountService = $discountService;
    }
        
    /**
     * Render checkout page.
     *
     * @Route("/cart/checkout", name="checkout")
     *
     * @param  mixed $request
     * @return void
     */
    public function checkout(Request $request): Response
    {
        $form = $this->createForm(CheckoutForm::class);
        $this->cartService->init($this->cartUtils);

        $discounts = $this->discountService->getDiscounts($this->cartUtils);
        $this->cartService->initDiscounts($discounts);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->checkoutService->setCustomer($form->getData());
            $this->checkoutService->setCart(serialize($this->cartService->dump()));
            $order = $this->checkoutService->persist();
            $this->cartUtils->clear();

            $this->addFlash(
                'notice',
                'Order completed!'
            );
            return $this->redirectToRoute('checkout_success', ['id' => $order->getId()]);
        }

        $total = $this->cartService->getTotalBeforeDiscounts();
        $finalTotal = $this->cartService->getTotal();
        return $this->render('checkout.html.twig', [
            "items" => $this->cartService->getItems(),
            "total" => $total,
            "finalTotal" => $finalTotal,
            "discount" => $total - $finalTotal,
            "coupon" => $this->cartUtils->getCoupon(),
            "checkoutForm" => $form->createView()
        ]);
    }
        
    /**
     * Render checkout success page.
     *
     * @Route("/cart/checkout/success/{id}", name="checkout_success")
     *
     * @param  mixed $id
     * @param  mixed $em
     * @return void
     */
    public function success(int $id, EntityManagerInterface $em): Response
    {
        $repository = $em->getRepository(Order::class);
        $order = $repository->find($id);
        $cartItems = unserialize($order->getCart());

        $itemIdAndCounts = [];
        foreach ($cartItems["items"] as $item) {
            $itemIdAndCounts[$item["itemId"]] = $item["count"];
        }
        $bookRepository = $em->getRepository(Book::class);
        $books = $bookRepository->findBy(["id" => array_keys($itemIdAndCounts)]);
        foreach ($books as $book) {
            $book->setCount($itemIdAndCounts[$book->getId()]);
        }

        return $this->render('checkout.success.html.twig', [
            "items" => $books,
            "order" => $order,
            "customer" => $order->getCustomer(),
            "cartItems" => $cartItems
        ]);
    }
}
