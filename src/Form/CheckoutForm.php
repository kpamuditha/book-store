<?php

namespace App\Form;

use DateTime;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Range;

class CheckoutForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $minExpireDate = new DateTime("now");
        $minExpireDate->modify('first day of this month');
        $minExpireDate->setTime(00, 00, 00);

        $builder
            ->add("first_name", TextType::class, [
                'constraints' => [new NotBlank(), new Length(['min' => 1])]
            ])
            ->add("last_name", TextType::class, [
                'constraints' => [new NotBlank(), new Length(['min' => 1])]
            ])
            ->add("email", EmailType::class, [
                'constraints' => [new NotBlank(), new Email()]
            ])
            ->add("address", TextType::class, [
                'constraints' => [new NotBlank(), new Length(['min' => 10])]
            ])
            ->add("name_on_card", TextType::class, [
                'constraints' => [new NotBlank(), new Length(['min' => 1])]
            ])
            ->add("credit_card_number", TextType::class, [
                'constraints' => [new NotBlank(), new Length(16)]
            ])
            ->add(
                'expire_date',
                DateType::class,
                [
                    "required" => true,
                    'format' => 'MMM-yyyy  d',
                    'years' => range(date('Y'), date('Y') + 12),
                    'days' => array(1),
                    'constraints' => [new NotBlank(), new Range(["min" => $minExpireDate])]
                ]
            )
            ->add("cvv", TextType::class, [
                'constraints' => [new NotBlank(), new Length(['max' => 5])]
            ]);
    }
}
