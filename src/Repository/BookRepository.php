<?php

namespace App\Repository;

use App\Entity\Book;
use BaseRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Book|null find($id, $lockMode = null, $lockVersion = null)
 * @method Book|null findOneBy(array $criteria, array $orderBy = null)
 * @method Book[]    findAll()
 * @method Book[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BookRepository extends BaseRepository
{
    private $meta;

    public function __construct(ManagerRegistry $registry, EntityManagerInterface $em)
    {
        parent::__construct($registry, Book::class);
        $this->meta = $em->getClassMetadata(Book::class);
    }
    
    /**
     * Search books.
     *
     * @param  mixed $criteria
     * @param  mixed $orderBy
     * @param  mixed $limit
     * @param  mixed $offset
     * @return QueryBuilder
     */
    public function search($criteria = [], $orderBy = null, $limit = null, $offset = null): QueryBuilder
    {
        $builder = $this->createQueryBuilder("b");

        $associations = $this->meta->associationMappings;

        foreach ($criteria as $field => $value) {
            if (array_key_exists($field, $associations)) {
                $builder
                    ->join("b.$field", $field)
                    ->andWhere("$field IN (:$field)")
                    ->setParameter($field, is_array($value) ? $value : [$value]);
            } else {
                $builder
                ->andWhere("$field = :$field")
                ->setParameter($field, $value);
            }
        }

        if ($orderBy) {
            $builder->addOrderBy($orderBy["column"], $orderBy["dir"]);
        }

        return $builder;
    }

    // /**
    //  * @return Book[] Returns an array of Book objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('b.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Book
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
