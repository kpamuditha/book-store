<?php

namespace App\Tests\Util;

use App\Entity\Book;
use App\Entity\Category;
use App\Service\CartService;
use App\Service\Discount\CategoryDiscounts;
use App\Service\Discount\CouponDiscounts;
use App\Service\Discount\EachCategoryDiscounts;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\TestCase;
use ReflectionClass;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class PriceCalculateTest extends TestCase
{
    private $em = null;
    private $si = null;

    /**
     * set up function.
     *
     * @return void
     */
    protected function setUp(): void
    {
        $this->em = $this->createMock(EntityManagerInterface::class);
        $this->si = $this->createMock(SessionInterface::class);
    }


    /**
     * Tear down function
     *
     * @return void
     */
    protected function tearDown(): void
    {
        $this->em = null;
        $this->si = null;
    }

    /**
     * Testing total price without any items and without any discounts.
     *
     * @return void
     */
    public function testTotalPriceWithoutItemsAndWithoutDiscounts(): void
    {
        $cartService = new CartService($this->em);
        $this->assertEquals($cartService->getTotal(), 0);
    }

    /**
     * Testing total price without any items and with one discount.
     *
     * @return void
     */
    public function testTotalPriceWithoutItemsAndWithOneDiscount(): void
    {
        $categoryDiscountable = $this->getCategoryDiscountable();
        $cartService = new CartService($this->em);
        $cartService->addDiscount($categoryDiscountable);
        $this->assertEquals($cartService->getTotal(), 0);
    }

    /**
     * Testing total price with one item.
     *
     * @return void
     */
    public function testTotalPriceWithOneItem(): void
    {
        $book1 = $this->createItem(1000, 1);
        $cartService = new CartService($this->em);
        $book1->setCount(1);
        $cartService->addItem($book1);
        $this->assertEquals($cartService->getTotal(), 1000);
    }

    /**
     * Testing total price with one item multiple added to the cart.
     *
     * @return void
     */
    public function testTotalPriceWithOneItemMultipleTimes(): void
    {
        $book1 = $this->createItem(1000, 1);
        $cartService = new CartService($this->em);
        $book1->setCount(2);
        $cartService->addItem($book1);
        $this->assertEquals($cartService->getTotal(), 1000 * 2);
    }

    /**
     * Testing total price with items.
     *
     * @return void
     */
    public function testTotalPriceWithItems(): void
    {
        $book1 = $this->createItem(1000, 1);
        $book1->setCount(1);

        $book2 = $this->createItem(2000, 2);
        $book2->setCount(1);

        $book3 = $this->createItem(3000, 1);
        $book3->setCount(1);

        $cartService = new CartService($this->em);
        $cartService->addItem($book1);
        $cartService->addItem($book2);
        $cartService->addItem($book3);

        $this->assertEquals($cartService->getTotal(), 6000);
    }

    /**
     * Testing total price with multiple items multiple times.
     *
     * @return void
     */
    public function testTotalPriceWithItemsMultipleTimes(): void
    {
        $book1 = $this->createItem(1000, 1);
        $book1->setCount(2);

        $book2 = $this->createItem(2000, 2);
        $book2->setCount(1);

        $book3 = $this->createItem(3000, 1);
        $book3->setCount(3);

        $cartService = new CartService($this->em);
        $cartService->addItem($book1);
        $cartService->addItem($book2);
        $cartService->addItem($book3);

        $this->assertEquals($cartService->getTotal(), (1000 * 2 + 2000 * 1 + 3000 * 3));
    }

    /**
     * Testing total price with items in one category and children discount applicable to it.
     *
     * @return void
     */
    public function testTotalPriceWithOneCategoryItemWithChildrenDiscountApllicable(): void
    {
        $book1 = $this->createItem(1000, 1);
        $book1->setCount(1);

        $book2 = $this->createItem(5000, 1);
        $book2->setCount(1);

        $book3 = $this->createItem(3000, 1);
        $book3->setCount(1);

        $book4 = $this->createItem(4000, 1);
        $book4->setCount(1);

        $book5 = $this->createItem(3000, 1);
        $book5->setCount(1);

        $cartService = new CartService($this->em);
        $cartService->addItem($book1);
        $cartService->addItem($book2);
        $cartService->addItem($book3);
        $cartService->addItem($book4);
        $cartService->addItem($book5);

        $childrenCatagoryDiscount = $this->getCategoryDiscountable(1, 5, 10, 3);
        $cartService->addDiscount($childrenCatagoryDiscount);

        $total = $book1->getPrice() + $book2->getPrice() + $book3->getPrice() + $book4->getPrice() + $book5->getPrice();
        $this->assertEquals($cartService->getTotal(), $total * 90 / 100);
    }

     /**
     * Testing total price with one item multiple times in cart and children discount applicable to it.
     *
     * @return void
     */
    public function testTotalPriceWithOneCategoryItemMultipleTimesWithChildrenDiscountApllicable(): void
    {
        $book1 = $this->createItem(1000, 1);
        $book1->setCount(5);

        $cartService = new CartService($this->em);
        $cartService->addItem($book1);

        $childrenCatagoryDiscount = $this->getCategoryDiscountable(1, 5, 10, 3);
        $cartService->addDiscount($childrenCatagoryDiscount);

        $total = $book1->getPrice() * 5;
        $this->assertEquals($cartService->getTotal(), $total * 90 / 100);
    }

    /**
     * Testing total price with items with two types of category discount applicable to it.
     *
     * @return void
     */
    public function testTotalPriceWithChildrenAndEachCategoryDiscountApllicable(): void
    {
        //chldren books
        $book1 = $this->createItem(1000, 1);
        $book1->setCount(1);

        $book2 = $this->createItem(5000, 1);
        $book2->setCount(1);

        $book3 = $this->createItem(3000, 1);
        $book3->setCount(1);

        $book4 = $this->createItem(6000, 1);
        $book4->setCount(1);

        $book5 = $this->createItem(2000, 1);
        $book5->setCount(1);

        //fiction books
        $book6 = $this->createItem(4000, 2);
        $book6->setCount(1);

        $book7 = $this->createItem(3000, 2);
        $book7->setCount(1);

        $book8 = $this->createItem(2000, 2);
        $book8->setCount(1);

        $book9 = $this->createItem(1000, 2);
        $book9->setCount(1);

        $book10 = $this->createItem(6000, 2);
        $book10->setCount(1);

        $cartService = new CartService($this->em);
        $cartService->addItem($book1);
        $cartService->addItem($book2);
        $cartService->addItem($book3);
        $cartService->addItem($book4);
        $cartService->addItem($book5);
        $cartService->addItem($book6);
        $cartService->addItem($book7);
        $cartService->addItem($book8);
        $cartService->addItem($book9);
        $cartService->addItem($book10);

        $childrenCatagoryDiscount = $this->getCategoryDiscountable(1, 5, 10, 3);
        $cartService->addDiscount($childrenCatagoryDiscount);

        $eachCatagoryDiscount = $this->getEachCategoryDiscountable(2, 5, 5, 2);
        $cartService->addDiscount($eachCatagoryDiscount);

        $childrenBookTotal = $book1->getPrice() + $book2->getPrice() + $book3->getPrice() + $book4->getPrice() + $book5->getPrice();
        $fictionBookTotal = $book6->getPrice() + $book7->getPrice() + $book8->getPrice() + $book9->getPrice() + $book10->getPrice();
        $total = $childrenBookTotal + $fictionBookTotal;
        $childrenDiscount = $childrenBookTotal * 10 / 100;
        $fictionDiscount =  $total * 5 / 100;

        $this->assertEquals($cartService->getTotal(), $total - ($childrenDiscount + $fictionDiscount));
    }

    /**
     * Testing total price with items with children category discount applicable and each category discount not applicable.
     *
     * @return void
     */
    public function testTotalPriceWithChildrenDiscountApplicableEachCategoryDiscountNotEligible(): void
    {
        //chldren books
        $book1 = $this->createItem(1000, 1);
        $book1->setCount(1);

        $book2 = $this->createItem(5000, 1);
        $book2->setCount(1);

        $book3 = $this->createItem(3000, 1);
        $book3->setCount(1);

        $book4 = $this->createItem(6000, 1);
        $book4->setCount(1);

        $book5 = $this->createItem(2000, 1);
        $book5->setCount(1);

        //fiction books
        $book6 = $this->createItem(4000, 2);
        $book6->setCount(1);

        $book7 = $this->createItem(3000, 2);
        $book7->setCount(1);

        $book8 = $this->createItem(2000, 2);
        $book8->setCount(1);

        $book9 = $this->createItem(1000, 2);
        $book9->setCount(1);

        // $book10 = $this->createItem(6000, 2);
        // $book10->setCount(1);

        $cartService = new CartService($this->em);
        $cartService->addItem($book1);
        $cartService->addItem($book2);
        $cartService->addItem($book3);
        $cartService->addItem($book4);
        $cartService->addItem($book5);
        $cartService->addItem($book6);
        $cartService->addItem($book7);
        $cartService->addItem($book8);
        $cartService->addItem($book9);
        // $cartService->addItem($book10);

        $childrenCatagoryDiscount = $this->getCategoryDiscountable(1, 5, 10, 3);
        $cartService->addDiscount($childrenCatagoryDiscount);

        $eachCatagoryDiscount = $this->getEachCategoryDiscountable(2, 5, 5, 2);
        $cartService->addDiscount($eachCatagoryDiscount);

        $childrenBookTotal = $book1->getPrice() + $book2->getPrice() + $book3->getPrice() + $book4->getPrice() + $book5->getPrice();
        $fictionBookTotal = $book6->getPrice() + $book7->getPrice() + $book8->getPrice() + $book9->getPrice();
        $total = $childrenBookTotal + $fictionBookTotal;
        $childrenDiscount = $childrenBookTotal * 10 / 100;

        $this->assertEquals($cartService->getTotal(), $total - $childrenDiscount);
    }

    /**
     * Testing total price with items coupon discount applicable to it.
     *
     * @return void
     */
    public function testTotalPriceWithCouponCode(): void
    {
        $book1 = $this->createItem(1000, 1);
        $book1->setCount(1);

        $book2 = $this->createItem(5000, 1);
        $book2->setCount(1);

        $book3 = $this->createItem(3000, 1);
        $book3->setCount(1);

        $book4 = $this->createItem(4000, 1);
        $book4->setCount(1);

        $book5 = $this->createItem(3000, 1);
        $book5->setCount(1);
        $total = $book1->getPrice() + $book2->getPrice() + $book3->getPrice() + $book4->getPrice() + $book5->getPrice();

        $cartService = new CartService($this->em);
        $cartService->addItem($book1);
        $cartService->addItem($book2);
        $cartService->addItem($book3);
        $cartService->addItem($book4);
        $cartService->addItem($book5);

        $couponDiscount = $this->getCouponDiscountable(15, 1);
        $cartService->addDiscount($couponDiscount);

        $this->assertEquals($cartService->getTotal(), $total * 85 / 100);
    }

    /**
     * Testing total price with items coupon discount applicable to it.
     *
     * @return void
     */
    public function testTotalPriceWithAllDiscountsApplicable(): void
    {
        //chldren books
        $book1 = $this->createItem(1000, 1);
        $book1->setCount(1);

        $book2 = $this->createItem(5000, 1);
        $book2->setCount(1);

        $book3 = $this->createItem(3000, 1);
        $book3->setCount(1);

        $book4 = $this->createItem(6000, 1);
        $book4->setCount(1);

        $book5 = $this->createItem(2000, 1);
        $book5->setCount(1);

        //fiction books
        $book6 = $this->createItem(4000, 2);
        $book6->setCount(1);

        $book7 = $this->createItem(3000, 2);
        $book7->setCount(1);

        $book8 = $this->createItem(2000, 2);
        $book8->setCount(1);

        $book9 = $this->createItem(1000, 2);
        $book9->setCount(1);

        $book10 = $this->createItem(6000, 2);
        $book10->setCount(1);

        $cartService = new CartService($this->em);
        $cartService->addItem($book1);
        $cartService->addItem($book2);
        $cartService->addItem($book3);
        $cartService->addItem($book4);
        $cartService->addItem($book5);
        $cartService->addItem($book6);
        $cartService->addItem($book7);
        $cartService->addItem($book8);
        $cartService->addItem($book9);
        $cartService->addItem($book10);

        $childrenBookTotal = $book1->getPrice() + $book2->getPrice() + $book3->getPrice() + $book4->getPrice() + $book5->getPrice();
        $fictionBookTotal = $book6->getPrice() + $book7->getPrice() + $book8->getPrice() + $book9->getPrice() + $book10->getPrice();
        $total = $childrenBookTotal + $fictionBookTotal;

        $childrenDiscount = $childrenBookTotal * 10 / 100;
        $fictionDiscount =  $total * 5 / 100;
        $couponDiscount = $total * 15 / 100;

        //Children category discount
        $childrenCatagoryDiscountable = $this->getCategoryDiscountable(1, 5, 10, 3);
        $cartService->addDiscount($childrenCatagoryDiscountable);

        //Discount each category
        $eachCatagoryDiscountable = $this->getEachCategoryDiscountable(2, 5, 5, 2);
        $cartService->addDiscount($eachCatagoryDiscountable);

        // Discount coupon code
        $couponDiscountable = $this->getCouponDiscountable(15);
        $cartService->addDiscount($couponDiscountable);

        $this->assertEquals($cartService->getTotal(), $total - $couponDiscount);
    }

    /**
     * Create a category discountable object.
     *
     * @return CategoryDiscounts
     */
    private function getCategoryDiscountable(
        int $categoryId = 1,
        int $minItemsTogetTheDiscount = 5,
        float $discountPercentage = 10,
        int $priority = 3
    ): CategoryDiscounts {
        $cd = new CategoryDiscounts();
        $childrenCategory = $this->getCategoryObject($categoryId);
        $cd->setCategory($childrenCategory);
        $cd->setMinItems($minItemsTogetTheDiscount);
        $cd->setDiscountPercentage($discountPercentage);
        $cd->setPriority($priority);

        return $cd;
    }

    /**
     * Create a category discountable object.
     *
     * @return CategoryDiscounts
     */
    private function getEachCategoryDiscountable(
        int $totalCategoryCount,
        int $minItemsTogetTheDiscount = 5,
        float $discountPercentage = 10,
        int $priority = 3
    ): EachCategoryDiscounts {
        $cd = new EachCategoryDiscounts();
        $cd->setMinItems($minItemsTogetTheDiscount);
        $cd->setDiscountPercentage($discountPercentage);
        $cd->setPriority($priority);
        $cd->setCategoryCount($totalCategoryCount);

        return $cd;
    }

    /**
     * Create a category discountable object.
     *
     * @return CategoryDiscounts
     */
    private function getCouponDiscountable(
        float $discountPercentage = 15
    ): CouponDiscounts {
        $cd = new CouponDiscounts();
        $cd->setDiscountPercentage($discountPercentage);
        $cd->setCouponCode("test");

        return $cd;
    }

    /**
     * Get category object.
     *
     * @param int $id
     * @return Category
     */
    private function getCategoryObject(int $categoryId): Category
    {
        $class = new ReflectionClass(Category::class);
        $property = $class->getProperty('id');
        $property->setAccessible(true);

        $ct = new Category();
        $property->setValue($ct, $categoryId);

        return $ct;
    }

    /**
     * Create an item (Book) object.
     *
     * @param  mixed $price
     * @param  mixed $categoryId
     * @return Book
     */
    private function createItem(float $price, int $categoryId): Book
    {
        $book = new Book();
        $book->setPrice($price);
        $book->setCategory($this->getCategoryObject($categoryId));

        return $book;
    }
}
