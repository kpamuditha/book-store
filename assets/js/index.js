const Index = () => {
  function appendToUrl(param, value) {
    let url = window.location.origin + window.location.pathname
    url += `?${param}=${value}`
    return url
  }

  document
    .getElementById('select-category')
    .addEventListener('change', function (event) {
      const category = parseInt(event.target.value)

      let location = ''
      if (category > 0) {
        location = appendToUrl('category', event.target.value)
      } else {
        location = window.location.origin + window.location.pathname
      }

      window.location = location
    })

  const cartElm = document.getElementById('cart')
  const priceElm = document.getElementById('total-price')

  function getCartPrice() {
    cartElm.classList.add('loading')
    fetch('/cart/total')
      .then((response) => response.json())
      .then((data) => {
        cartElm.classList.remove('loading')
        if (data.success) {
          priceElm.textContent = `$${data.total}`
        }
      })
  }

  getCartPrice()

  function addToCart(event) {
    event.preventDefault()
    const elm = event.target
    const text = elm.textContent
    elm.innerHTML =
      '<div class="spinner-border spinner-border-sm" role="status" style=""><span class="sr-only">Loading...</span></div>'

    elm.classList.add('add-in-progress')
    const bookId = this.dataset.book
    fetch(`/cart/add/${bookId}`)
      .then((response) => response.json())
      .then((data) => {
        event.target.classList.remove('add-in-progress')
        if (data.success) {
          getCartPrice()
        }
        elm.textContent = text
      })
  }

  const addToCartsElms = document.getElementsByClassName('btn-add-to-cart')

  Array.from(addToCartsElms).forEach(function (element) {
    element.addEventListener('click', addToCart)
  })
}

export default Index
